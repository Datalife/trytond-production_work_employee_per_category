# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

__all__ = ['LaborYieldAllocation', 'LaborYieldAllocate']


class LaborYieldAllocation(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    def get_work_center(self, name):
        cat_id = (self.work.center_category.id if
            self.work and self.work.center_category else None)
        return self.employee.compute_work_center(
            date=self.date, center_category=cat_id) if self.employee else None


class LaborYieldAllocate(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocate'

    def _get_employee_work_centers(self, employees):
        cat_id = (self.procedure.procedure.work.center_category.id
            if self.procedure.procedure.work and
            self.procedure.procedure.work.center_category else None)
        with Transaction().set_context(date=self.get_params.date,
                center_category=cat_id):
            return {e.id: e.work_center for e in employees}
