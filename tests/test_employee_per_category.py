# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.exceptions import UserError
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
import datetime
from trytond.modules.company.tests import create_company, set_company


class ProductionWorkEmployeePerCategoryTestCase(ModuleTestCase):
    """Test Production Work Employee Per Category module"""
    module = 'production_work_employee_per_category'

    def configure_work_centers(self):
        pool = Pool()
        Location = pool.get('stock.location')
        WorkCenter = pool.get('production.work.center')
        Category = pool.get('production.work.center.category')

        cat1 = Category(name='Category 1')
        cat1.save()
        cat2 = Category(name='Category 2')
        cat2.save()
        warehouse, = Location.search([('type', '=', 'warehouse')])
        wcenter1 = WorkCenter(name='Work Center 1', warehouse=warehouse,
            category=cat1)
        wcenter1.save()
        wcenter2 = WorkCenter(name='Work Center 2', warehouse=warehouse,
            category=cat2)
        wcenter2.save()
        wcenter3 = WorkCenter(name='Work Center 3', warehouse=warehouse,
            category=cat1)
        wcenter3.save()

        work_centers = [
            (datetime.date(2014, 1, 1), None, wcenter1),
            (datetime.date(2015, 1, 1), datetime.date(2015, 12, 31), wcenter2),
            (datetime.date(2016, 1, 1), datetime.date(2016, 12, 31), wcenter3),
            (datetime.date(2014, 1, 2), datetime.date(2014, 1, 20), wcenter2),
            (datetime.date(2016, 1, 2), datetime.date(2016, 1, 2), wcenter2)
        ]
        test_work_centers = [
            (datetime.date(2013, 1, 1), None),
            (datetime.date(2014, 1, 1), wcenter1),
            (datetime.date(2014, 1, 5), wcenter1),
            (datetime.date(2014, 1, 5), wcenter2),
            (datetime.date(2014, 6, 1), wcenter1),
            (datetime.date(2015, 1, 1), wcenter2),
            (datetime.date(2015, 6, 1), wcenter2),
            (datetime.date(2016, 1, 1), wcenter3),
            (datetime.date(2016, 1, 2), wcenter2),
            (datetime.date(2016, 1, 3), wcenter3),
            (datetime.date(2016, 6, 1), wcenter3)]
        return work_centers, test_work_centers

    @with_transaction()
    def test_compute_employee_work_center(self):
        """Test compute_work_center"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')
        WorkCenter = pool.get('production.work.center')

        company = create_company()
        with set_company(company):
            work_centers, test_work_centers = self.configure_work_centers()
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()
            for date, end_date, work_center in work_centers:
                res = EmployeeWorkCenter(
                    employee=employee,
                    date=date,
                    end_date=end_date,
                    work_center=work_center)
                res.save()
            for date, work_center in test_work_centers:
                cat_id = (work_center and work_center.category and
                    work_center.category.id or None)
                self.assertEqual(employee.compute_work_center(date,
                    center_category=cat_id),
                    work_center and work_center.id or None)
                if work_center:
                    self.assertEqual(WorkCenter.compute_employee(
                        [work_center], date), {work_center.id: employee.id})

    @with_transaction()
    def test_employee_work_center_restriction_default(self):
        """Test employee work center restriction"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')

        company = create_company()
        with set_company(company):
            work_centers, _ = self.configure_work_centers()
            # set invalid default date
            work_centers[1] = (
                datetime.date(2015, 1, 1), None, work_centers[1][2])
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()

            date, end_date, work_center = work_centers[0]
            EmployeeWorkCenter(
                employee=employee,
                date=date,
                end_date=end_date,
                work_center=work_center).save()
            date, end_date, work_center = work_centers[1]
            res = EmployeeWorkCenter(
                employee=employee,
                date=date,
                end_date=end_date,
                work_center=work_center)
            res.save()
            res.work_center = work_centers[2][2]
            self.assertRaises(UserError, res.save)

    @with_transaction()
    def test_employee_work_center_category_restriction(self):
        """Test employee work center restriction"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')

        company = create_company()
        with set_company(company):
            work_centers, _ = self.configure_work_centers()
            # set another work center of same category
            work_centers[4] = (
                datetime.date(2015, 12, 31),
                datetime.date(2016, 1, 2)) + (work_centers[0][2], )
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()

            for date, end_date, work_center in work_centers[:4]:
                EmployeeWorkCenter(
                    employee=employee,
                    date=date,
                    end_date=end_date,
                    work_center=work_center).save()
            date, end_date, work_center = work_centers[4]
            res = EmployeeWorkCenter(
                employee=employee,
                date=date,
                end_date=end_date,
                work_center=work_center)
            self.assertRaises(UserError, res.save)

    @with_transaction()
    def test_employee_work_center_restriction_employee(self):
        """Test employee work center restriction"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')

        company = create_company()
        with set_company(company):
            work_centers, _ = self.configure_work_centers()
            # set invalid default date
            wc1 = work_centers[0][2]
            employees = []
            for _name in ('Pam', 'John', 'Paul', 'Shara'):
                party = Party(name='%s Beesly' % _name)
                party.save()
                employee = Employee(party=party.id, company=company)
                employee.save()
                employees.append(employee)

            ii = 0
            for date, end_date, _ in work_centers:
                EmployeeWorkCenter(
                    employee=employees[ii % 4],
                    date=date,
                    end_date=end_date,
                    work_center=wc1).save()
                ii += 1

    @with_transaction()
    def test_employee_work_center_restriction_employee2(self):
        """Test employee work center restriction"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')

        company = create_company()
        with set_company(company):
            work_centers, _ = self.configure_work_centers()
            # set invalid default date
            wc1 = work_centers[0][2]
            work_centers[2] = (datetime.date(2015, 6, 30),
            datetime.date(2016, 1, 31), work_centers[2][2])
            employees = []
            for _name in ('Pam', 'John', 'Paul', 'Shara'):
                party = Party(name='%s Beesly' % _name)
                party.save()
                employee = Employee(party=party.id, company=company)
                employee.save()
                employees.append(employee)

            ii = 0
            for date, end_date, _ in work_centers[:2]:
                EmployeeWorkCenter(
                    employee=employees[ii % 4],
                    date=date,
                    end_date=end_date,
                    work_center=wc1).save()
                ii += 1
            date, end_date, _ = work_centers[2]
            res = EmployeeWorkCenter(
                employee=employee,
                date=date,
                end_date=end_date,
                work_center=wc1)
            self.assertRaises(UserError, res.save)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductionWorkEmployeePerCategoryTestCase))
    return suite
