# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    def get_work_center(self, name=None):
        work_center = self.compute_work_center(
            Transaction().context.get('date', None),
            center_category=Transaction().context.get('center_category', None))
        return work_center

    @classmethod
    def _get_work_center_cache_key(cls, employee_wc):
        res = super(Employee, cls)._get_work_center_cache_key(employee_wc)
        return res + (employee_wc.work_center.category.id if
            employee_wc.work_center.category else None, )

    @classmethod
    def search_work_center(cls, name, clause):
        res = super(Employee, cls).search_work_center(name, clause)
        if Transaction().context.get('center_category', None):
            res[0][2].append(
                ('work_center.category', '=',
                    Transaction().context['center_category']))
        return res

    def compute_work_center(self, date=None, **kwargs):
        pool = Pool()
        Date = pool.get('ir.date')

        cat_id = kwargs.get('center_category', None)
        employee_work_centers = self.get_work_centers()

        if date is None:
            date = Date.today()
        # search the work center for the given date
        work_center = None
        if employee_work_centers and date >= employee_work_centers[0][0]:
            for key in employee_work_centers:
                edate, eend_date, ework_center, wc_category = key
                if date >= edate:
                    if date <= eend_date:
                        if not cat_id or wc_category == cat_id:
                            work_center = ework_center
                else:
                    break
        return work_center


class EmployeeWorkCenter(metaclass=PoolMeta):
    __name__ = 'company.employee-work.center'

    @classmethod
    def _check_restriction(cls, records, field_name):
        if field_name == 'work_center':
            return super(EmployeeWorkCenter, cls)._check_restriction(
                records, field_name)

        values = {}
        # {employee_id: {(date1,date2): {category_id: record_id}}}
        for record in records:
            if not record.work_center.category:
                continue
            _field_value = getattr(record, field_name).id
            values.setdefault(_field_value, {})
            _key = (record.date, record.end_date)
            if not record.end_date:
                _key = 'default'
            if values[_field_value].get(_key):
                if record.work_center.category.id in values[
                        _field_value][_key]:
                    raise UserError(gettext(
                        'production_work_employee_per_category.'
                        'msg_employee_workcenter_unique'))
            if record.end_date:
                for _, _date_values in values.items():
                    for date_keys, cat_ids in _date_values.items():
                        if not isinstance(date_keys, tuple):
                            continue
                        if ((record.date <= date_keys[1] <= record.end_date and
                                record.date > date_keys[0]) or
                            (record.end_date >= date_keys[0] >= record.date and
                                record.end_date < date_keys[1])):
                            if record.work_center.category.id in cat_ids:
                                raise UserError(gettext(
                                    'production_work_employee_per_category.'
                                    'msg_employee_workcenter_unique'))
            values[_field_value].setdefault(_key, {
                record.work_center.category.id: record.id})

        # check persisted data
        for field_id, _date_values in values.items():
            _domain = [(field_name, '=', field_id)]
            ids = []
            subdomain = ['OR', ]
            for _key, cat_ids in _date_values.items():
                ids.extend(list(cat_ids.values()))
                if _key == 'default':
                    subdomain.append([
                        ('end_date', '=', None),
                        ('work_center.category', 'in', list(cat_ids.keys()))])
                else:
                    subdomain.append([
                        ('date', '<=', _key[1]),
                        ('end_date', '>=', _key[1]),
                        ('date', '>', _key[0]),
                        ('work_center.category', 'in', list(cat_ids.keys()))])
                    subdomain.append([
                        ('date', '<=', _key[0]),
                        ('end_date', '>=', _key[0]),
                        ('end_date', '<', _key[1]),
                        ('work_center.category', 'in', list(cat_ids.keys()))])
            if ids:
                _domain.append(('id', 'not in', ids))
            if subdomain:
                _domain.append(subdomain)

            res = cls.search(_domain)
            if res:
                raise UserError(gettext(
                    'production_work_employee_per_category.'
                    'msg_employee_workcenter_unique'))
